= Getting Started With Spring REST Docs
Dabin Ding;
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:

[introduction]
= Introduction
This is an example output for a service running at http://localhost:8080:


[getting-started]
= Getting started



[getting-started-running-the-service]
== Running the service
RESTful Notes is written using http://projects.spring.io/spring-boot[Spring Boot] which
makes it easy to get it up and running so that you can start exploring the REST API.

The first step is to clone the Git repository:

[source,bash]
----
$ git clone https://github.com/spring-projects/spring-restdocs
----

Once the clone is complete, you're ready to get the service up and running:

[source,bash]
----
$ cd samples/rest-notes-spring-data-rest
$ ./mvnw clean package
$ java -jar target/*.jar
----

You can check that the service is up and running by executing a simple request using
cURL:

include::{snippets}/index/1/curl-request.adoc[]

This request should yield the following response in the
http://stateless.co/hal_specification.html[Hypertext Application Language (HAL)] format:

include::{snippets}/index/1/http-response.adoc[]

Note the `_links` in the JSON response. They are key to navigating the API.



[Services API]
= Services API
.request
include::{snippets}/user/http-request.adoc[]

.response
include::{snippets}/user/http-response.adoc[]

.response-fields
include::{snippets}/user/response-fields.adoc[]

As you can see the format is very simple, and in fact you always get the same message.