package edu.ucmo.cs.unittesting.mockmvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;

import java.util.Optional;


@RestController
@RequestMapping("/api")
public class UserRestController {

    @Autowired
    private UserRepository userRepository;

    public void setUserRepository(UserRepository uRepo){
        userRepository=uRepo;
    }

    @RequestMapping(path="/all",method = RequestMethod.GET,produces ="application/json")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        // This returns a JSON with the users
        return userRepository.findAll();
    }

    @RequestMapping("/greeting")
    public String greeting(){
        return "Hello World!";
    }

    @RequestMapping("/")
    String index(){
        return "index";
    }


    @RequestMapping(path="/user/{uid}",method = RequestMethod.GET,produces ="application/json") // Map ONLY GET Requests
    @ResponseBody
    public String getUserByID (@PathVariable(value = "uid") Long uid) {

        Optional<User> cur=userRepository.findById(uid);
        if(!cur.isPresent())
            return "{\"result\":\"user not found\"}";
        else {
            User user=cur.get();
            return "{\"id\":" + user.getId() + "," +
                    "\"name\":\"" + user.getName() + "\"," +
                    "\"email\":\"" + user.getEmail() + "\"" +
                    "}";
        }
    }
}