package edu.ucmo.cs.unittesting;

import edu.ucmo.cs.unittesting.junit.MyMath;
import org.junit.*;

import static org.junit.Assert.assertEquals;

public class JunitTests {
    MyMath myMath = new MyMath();

    //Run before and after every test method in the class
    @Before
    public void before() {
        System.out.println("Before cases");
    }
    @After
    public void after() {
        System.out.println("After cases");
    }

    //Static methods which are executed once before and after a test class
    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before test Class ");
    }
    @AfterClass
    public static void afterClass() {
        System.out.println("After test Class");
    }

    // MyMath.sum
    // 1,2,3 => 6
    @Test
    public void sum_with3numbers() {
        System.out.println("Test1");
        assertEquals(6, myMath.sum(new int[] {
                1,
                2,
                3
        }));
    }
    @Test
    public void sum_with1number() {
        System.out.println("Test2");
        assertEquals(3, myMath.sum(new int[] {
                3
        }));
    }
}
