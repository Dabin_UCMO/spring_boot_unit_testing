package edu.ucmo.cs.unittesting;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.templates.TemplateFormats;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;




import static org.mockito.Mockito.when;

import edu.ucmo.cs.unittesting.mockmvc.User;
import edu.ucmo.cs.unittesting.mockmvc.UserRepository;
import edu.ucmo.cs.unittesting.mockmvc.UserRestController;
import org.junit.Before;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RunWith(SpringRunner.class)
@SpringBootTest

public class MockMvcTests {


	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

	private MockMvc mockMvc;
	private User user1;


	@Autowired
	private UserRestController controller;

	@MockBean
	private UserRepository userRepo;


	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(controller).apply(documentationConfiguration(this.restDocumentation))
				.build();
		user1 = new User();
		user1.setId(1L);
		user1.setName("John Doe");
		user1.setEmail("test@test.com");
		Optional<User> opt= Optional.of(user1);
		when(userRepo.findById(1L)).thenReturn(opt);

	}

	@Test
	public void contexLoads() {
		assertThat(controller).isNotNull();
	}



	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/api/greeting")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello World!")));
	}


	@Test
	public void testIndex() throws Exception{
		this.mockMvc.perform(get("/api/"))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("index")))
				.andDo(print())
				.andDo(document("api"))
				 ;
	}

	//Test UserController
	@Test
	public void testGetUser() throws Exception{

		this.mockMvc.perform(get("/api/user/1"))
				//.contentType(MediaType.APPLICATION_JSON)
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name",is(user1.getName())))
				.andDo(print())
				.andDo(document("user", responseFields(
						fieldWithPath("email")
								.description("The user's email address"),
						fieldWithPath("name").description("The user's name"),
						fieldWithPath("id").description("The user's id"))))



				;
	}



	@Test
	public void testGetAllUser() throws Exception{
		User user2=new User();
		user2.setId(2L);
		user2.setName("Tayler Swift");
		user2.setEmail("test@test.com");
		List<User> allUsers = Arrays.asList(user1, user2);
		given(userRepo.findAll()).willReturn(allUsers);
		this.mockMvc.perform(get("/api/all"))

				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", hasSize(2)))

				.andExpect(jsonPath("$[0].name",is(user1.getName())))
				.andExpect(jsonPath("$[1].name",is(user2.getName())))
				.andDo(print());
		verify(userRepo, VerificationModeFactory.times(1)).findAll();
	}



}
