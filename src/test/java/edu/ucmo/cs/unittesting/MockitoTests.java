package edu.ucmo.cs.unittesting;


import edu.ucmo.cs.unittesting.mockito.BusinessService;
import edu.ucmo.cs.unittesting.mockito.DataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

//Testing on the business service class
@RunWith(SpringRunner.class)
@SpringBootTest
public class MockitoTests {
        @MockBean
        DataService dataServiceMock;
        @Autowired
        BusinessService businessImpl;

        @Before
        public void setup(){

        }
        @Test
        public void testFindTheGreatestFromAllData() {
            when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {
                    24,
                    15,
                    3
            });
            assertEquals(24, businessImpl.findTheGreatestFromAllData());
        }
        @Test
        public void testFindTheGreatestFromAllData_ForOneValue() {
            when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {
                    15
            });
            assertEquals(15, businessImpl.findTheGreatestFromAllData());
        }
        @Test
        public void testFindTheGreatestFromAllData_NoValues() {
            when(dataServiceMock.retrieveAllData()).thenReturn(new int[] {});
            assertEquals(Integer.MIN_VALUE, businessImpl.findTheGreatestFromAllData());
        }
    }

